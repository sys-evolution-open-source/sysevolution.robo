﻿// {Nome: "Prefeitura de Quirinópolis - GO"}

using Newtonsoft.Json.Linq;
using SysEvolution.Robo.Worker.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development.Scripts.Edital
{
    public class PrefeituraQuirinopolisGO : Spider
    {
        protected override void Configure()
        {
            Initialize();
        }

        protected override void Run()
        {
            // Obtém o html da página
            var html = GetHtml("http://www.quirinopolis.go.gov.br/licitacao.php?ano=2020");
            // Obtém as linhas de edital
            var linhas = html.DocumentNode.QuerySelectorAll("#modallall > table.results > tbody > tr");

            // Itera sobre cada linha de licitação da página
            for (var i = 0; i < linhas.Count; i++)
            {
                var linha = linhas[i];
                var numeroEdital = linha.QuerySelector("td:first-child").InnerText;
                var objeto = linha.QuerySelector("td:nth-child(2)").InnerText;
                var modalidade = linha.QuerySelector("td:nth-child(3)").InnerText;
                var dataHoraCertame = linha.QuerySelector("td:nth-child(4)").InnerText;
                var licitador = "Prefeitura Municipal de Quirinópolis - GO";

                var licitacao = new JObject();
                licitacao["NumeroEdital"] = numeroEdital;
                licitacao["Objeto"] = objeto;
                licitacao["Modalidade"] = modalidade;
                licitacao["DataHoraCertame"] = dataHoraCertame;
                licitacao["Licitador"] = licitador;
            }
        }
    }
}
