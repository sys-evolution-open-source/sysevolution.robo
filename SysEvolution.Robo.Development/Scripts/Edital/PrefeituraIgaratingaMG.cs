﻿// {Nome: "Prefeitura de Igaratinga - MG"}

using SysEvolution.Robo.Worker.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development.Scripts.Edital
{
    public class PrefeituraIgaratingaMG : Spider
    {
        protected override void Configure()
        {
            Initialize();
        }

        protected override void Run()
        {
            // Obtém o html da página 1
            var html = GetHtml("http://igaratinga.mg.gov.br/arquivo/licitacoes?pagina=1&quantidade=10&Tipo=0");

            // Obtém as linhas de edital
            var linhas = html.DocumentNode.QuerySelectorAll("section > article > div");

            // Itera sobre cada linha de licitação da página

            // Extrai as licitações e grava cada uma em formato json
        }
    }
}
