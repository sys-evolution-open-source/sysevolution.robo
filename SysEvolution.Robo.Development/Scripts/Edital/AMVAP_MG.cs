﻿using SysEvolution.Robo.Worker.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development.Scripts.Edital
{
    public class AMVAP_MG : Generic
    {

        protected override void Configure()
        {
            Field(EnumChamado.Estado, "MG");
            Field(EnumChamado.Cidade, "Uberlândia");
            Field(EnumChamado.Orgao, "AMVAP");




        }

    }
}
