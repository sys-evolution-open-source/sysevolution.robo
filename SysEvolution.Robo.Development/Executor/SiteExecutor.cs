﻿using Newtonsoft.Json.Linq;
using SysEvolution.Robo.Util;
using SysEvolution.Robo.Worker.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development.Executor
{
    public class SiteExecutor
    {
        private string root;
        private bool stopped;
        private List<Site> runningSites;

        public SiteExecutor()
        {
            runningSites = new List<Site>();

            int idWorker = 0;
        }


        public void Run(string scriptName, string siteName)
        {
            var sites = Compile(scriptName);

            if (string.IsNullOrEmpty(siteName))
            {
                foreach (var site in sites)
                {
                    if (stopped)
                        break;

                    RunSite(site);
                }
            }
            else
                RunSite(sites.First(_ => _.Nome == siteName));

            stopped = false;
        }

        public void RunSite(Site site)
        {
            Logger.Log("[{0}][{1}] Iniciado", site.Script.Nome, site.Nome);

            try
            {
                site.DataHoraInicioExecucao = DateTime.Now;
                runningSites.Add(site);
                site.Spider.Start();
                site.DataHoraFimExecucao = DateTime.Now;
                Logger.Log("[{0}][{1}] Finalizado com sucesso", site.Script.Nome, site.Nome);
                runningSites.Remove(site);
            }
            catch (Exception e)
            {
                site.DataHoraFimExecucao = DateTime.Now;
                Logger.Log(e);
                Logger.Log("[{0}][{1}] Finalizado com erro", site.Script.Nome, site.Nome);
                runningSites.Remove(site);
            }
        }

        public List<Site> Compile(string scriptName)
        {
            var tipoScript = scriptName.StartsWith("H_") ? "Homologacao" : "Edital";

            var dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent;
            var fileName = Path.Combine(dir.FullName, "Scripts", tipoScript, scriptName + ".cs");
            var content = File.ReadAllText(fileName);

            var script = new JObject();
            script["Nome"] = scriptName;
            script["Codigo"] = content;

            var sites = new List<Site>();

            var sitesJson = ExtrairSites(content);
            foreach (var siteJson in sitesJson)
            {
                var site = new Site()
                {
                    Nome = (string)siteJson["Nome"],
                    Script = new Script()
                    {
                        Nome = scriptName,
                        Tipo = TipoScript.Edital
                    }
                };

                var type = Type.GetType("SysEvolution.Robo.Development.Scripts." + tipoScript + "." + scriptName);
                var spider = (Worker.Core.Spider)Activator.CreateInstance(type);

                spider.SiteName = (string)siteJson["Nome"];
                spider.TempoExecucao = 30;

                site.Spider = spider;

                sites.Add(site);
            }
            
            return sites;
        }

        private List<JObject> ExtrairSites(string codigo)
        {
            var sites = new List<JObject>();

            var lines = Regex.Split(codigo, "\r\n|\r|\n");

            foreach (var line in lines)
            {
                if (line.StartsWith("// {"))
                {
                    var match = Regex.Match(line, @"// (?<json>.*)");
                    var parameters = JObject.Parse(match.Groups["json"].Value);

                    var site = new JObject();
                    site["Nome"] = parameters["Nome"];
                    parameters.Remove("Nome");
                    site["Parametros"] = parameters;

                    sites.Add(site);
                }
                else
                    break;
            }

            return sites;
        }

        public List<string> GetAllScripts()
        {
            var dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent;
            var pathEdital = Path.Combine(dir.FullName, "Scripts", "Edital");

            var filesEdital = Directory.GetFiles(pathEdital);

            var files = filesEdital.Select(Path.GetFileNameWithoutExtension).ToList();

            return files;
        }

        public List<string> GetAllSites(string scriptName)
        {
            var sites = new List<string>();
            var dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent;
            var fileName = Path.Combine(dir.FullName, "Scripts", "Edital", scriptName + ".cs");

            // se o arquivo não existe, provavelmente é site de resultado
            if (File.Exists(fileName))
            {
                var lines = File.ReadAllLines(fileName);

                foreach (var line in lines)
                {
                    var parameters = new JObject();

                    if (line.StartsWith("// {"))
                    {
                        var match = Regex.Match(line, @"// (?<json>.*)");
                        parameters = JObject.Parse(match.Groups["json"].Value);

                        sites.Add((string)parameters["Nome"]);
                    }
                    else
                        break;
                }
            }

            return sites;
        }
    }
}
