﻿using SysEvolution.Robo.Util;
using SysEvolution.Robo.Worker.Core;
using SysEvolution.Robo.Worker.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var executor = new Executor();

            while (true)
            {
                Console.Write(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ">");
                var line = Console.ReadLine();

                var tokens = line.Split(' ');

                if (tokens.Count() > 0)
                {
                    var command = tokens[0].ToLower();

                    switch (command)
                    {
                        case "run":
                            try
                            {
                                RunScript(tokens[1]);
                            } catch(Exception e)
                            {
                                Logger.Log("Script inválido: 'run nomeScript'");
                            }
                            if (tokens.Count() == 1)
                            {
                                Logger.Log("Comando inválido: 'run nomeScript'");
                            } else
                            {
                                var scriptName = tokens[1];
                                var type = Type.GetType("SysEvolution.Robo.Development.Scripts.Edital." + scriptName);
                                var instance = (Worker.Core.Spider)Activator.CreateInstance(type);
                                Logger.Log("Execução iniciada {0}", scriptName);
                                instance.Start();
                                Logger.Log("Execução finalizada");
                            }

                            break;
                        case "publish":
                            var scriptName = tokens[1];
                            Logger.Log("Publicando script {0}", scriptName);
                            break;
                    }

                    if (command.ToLower().StartsWith("run"))
                    {
                        var tokens = command.Split(' ');
                        var scriptName = tokens[1];

                        
                    }
                    else if (command.ToLower().StartsWith("publish"))
                    {
                        var tokens = command.Split(' ');
                        var scriptName = tokens[1];

                        Logger.Log("Publicando script {0}", scriptName);
                    }
                    Logger.Log("Comando inválido, digite 'help' para visualizar os comandos disponíveis");
                }
            }
        }
    }
}
