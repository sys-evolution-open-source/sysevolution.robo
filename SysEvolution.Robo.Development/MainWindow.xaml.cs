﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SysEvolution.Robo.Development
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Console console = Console.Instance;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = console;
            Loaded += MainWindow_Loaded;
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            InputBlock.KeyDown += InputBlock_KeyDown;
            InputBlock.PreviewKeyDown += InputBlock_PreviewKeyDown;
            InputBlock.Focus();

            console.Log("Para mais informações digite 'help'");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Scroller.ScrollToBottom();
        }

        void InputBlock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                console.RunCommand(InputBlock.Text);
                InputBlock.Text = string.Empty;
                InputBlock.Focus();
            }
            else if (e.Key == Key.Tab)
            {
                var command = console.TryAutoComplete(InputBlock.Text);
                InputBlock.Text = command; 
                InputBlock.SelectionStart = command.Length;
                InputBlock.SelectionLength = 0;
                e.Handled = true;
            }
        }

        void InputBlock_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                var command = console.PrevCommand();

                if (!string.IsNullOrEmpty(command))
                {
                    InputBlock.Text = command;
                    InputBlock.SelectionStart = command.Length;
                    InputBlock.SelectionLength = 0;
                }
            } else if (e.Key == Key.Down)
            {
                var command = console.NextCommand();

                if (!string.IsNullOrEmpty(command))
                {
                    InputBlock.Text = command;
                    InputBlock.SelectionStart = command.Length;
                    InputBlock.SelectionLength = 0;
                }
            } else if (e.Key == Key.Escape)
            {
                InputBlock.Text = "";
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            InputBlock.Focus();
        }
    }
}
