﻿using SysEvolution.Robo.Development.Executor;
using SysEvolution.Robo.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Development
{
    public class Console : INotifyPropertyChanged
    {
        #region static methods
        private static Console instance;
        public static Console Instance
        {
            get
            {
                if (instance == null)
                    instance = new Console();

                return instance;
            }
        }

        public static void Write(string log)
        {
            instance.Output = log;
        }

        #endregion

        #region properties

        int index;
        string consoleLogFileName;
        List<string> consoleLog;
        StringBuilder output;
        public string Output
        {
            get
            {
                return output.ToString();
            }
            set
            {
                if (output.Length > 30000)
                    output.Remove(0, 15000);

                output.Append("\r\n" + value);
                if (output.ToString().StartsWith("\r\n"))
                    output = output.Remove(0, 2);
                if (output.ToString().EndsWith("\r\n"))
                    output = output.Remove(output.Length - 2, 2);
                OnPropertyChanged("Output");
            }
        }

        SiteExecutor siteExecutor;
        List<Task> tasks;

        #endregion

        public Console()
        {
            consoleLogFileName = Path.Combine("console.log");

            if (File.Exists(consoleLogFileName))
            {
                consoleLog = File.ReadAllLines(consoleLogFileName).ToList();
            }
            else
            {
                File.Create(consoleLogFileName).Dispose();
                consoleLog = new List<string>();
            }

            index = consoleLog.Count;

            output = new StringBuilder();
            
            siteExecutor = new SiteExecutor();

            Logger.TextAppended += Log;

            tasks = new List<Task>();
        }

        public void RunCommand(string commandLine)
        {
            var parameters = commandLine.Split(' ');
            var command = parameters[0];

            Logger.Log("Comando: " + commandLine);

            switch (command)
            {
                case "run":
                    Run(commandLine);
                    break;
                case "clear":
                case "cls":
                    Clear();
                    break;
                case "help":
                    Help();
                    break;
                default:
                    InvalidCommand(commandLine);
                    break;
            }

            LogCommand(commandLine);
        }

        public void Run(string commandLine)
        {
            string scriptName = string.Empty, siteName = string.Empty;
            
            var match = Regex.Match(commandLine, @"run (?<script>.*?)$");

            if (match.Success)
                scriptName = match.Groups["script"].Value;

            if (!siteExecutor.GetAllScripts().Contains(scriptName))
                Logger.Log("Script \"{0}\" inválido", scriptName);
            else if (!string.IsNullOrEmpty(siteName) && !siteExecutor.GetAllSites(scriptName).Contains(siteName))
                Logger.Log("Site \"{0}\" inválido", scriptName);
            else
            {
                Logger.Log("[{0}] Iniciado", scriptName);
                var newTask = Task.Factory.StartNew(() => siteExecutor.Run(scriptName, siteName));
                tasks.Add(newTask);

                newTask.ContinueWith(_ =>
                {
                    tasks.Remove(newTask);
                    Logger.Log("[{0}] Finalizado", scriptName);
                });
            }
        }

        public void Clear()
        {
            output = new StringBuilder();
            OnPropertyChanged("Output");
        }

        public void Help()
        {
            Logger.Log("");
            Logger.Log("Lista de comandos:");
            Logger.Log("run NomeScript [\"NomeSite\"]: Inicializa o script 'NomeScript', se o parâmetro \"NomeSite\" não for definido, todos os sites serão executados");
            Logger.Log("cls/clear: Limpa a linha de comando");
            Logger.Log("");
        }

        public void InvalidCommand(string commandLine)
        {
            Logger.Log("Atenção: Comando '" + commandLine + "' não reconhecido. Digite 'help' para ver todos os comandos");
        }

        public string TryAutoComplete(string commandLine)
        {
            var match = Regex.Match(commandLine, @"run (?<script>.*?) (?<site>.*?)$");

            string command, scriptName, siteName;

            if (match.Success)
            {
                scriptName = match.Groups["script"].Value;
                siteName = match.Groups["site"].Value;

                var sites = siteExecutor.GetAllSites(scriptName);

                var filtered = sites.Where(_ => _.ToLower().Contains(siteName.ToLower())).ToList();

                if (filtered.Count == 1)
                    return string.Format("run {0} \"{1}\"", scriptName, filtered.First());
            }
            else
            {
                match = Regex.Match(commandLine, @"(?<command>.*?) (?<script>.*?)$");

                if (match.Success)
                {
                    command = match.Groups["command"].Value;
                    scriptName = match.Groups["script"].Value;

                    var scripts = siteExecutor.GetAllScripts();

                    var filtered = scripts.Where(_ => _.ToLower().Contains(scriptName.ToLower())).ToList();

                    if (filtered.Count == 1) 
                        return string.Format("{0} {1}", command, filtered.First());
                    else
                    {
                        filtered = filtered.Where(_ => _.ToLower().StartsWith(scriptName.ToLower())).ToList();

                        if (filtered.Count == 1)
                            return string.Format("{0} {1}", command, filtered.First());
                    }
                }
            }

            return commandLine;
        }

        public void Log(object sender, string log)
        {
            Output = log;
        }

        public void Log(string log)
        {
            Output = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - " + log;
        }

        public void EmptyLine()
        {
            Output = "\r\n";
        }

        void LogCommand(string command)
        {
            if (!string.IsNullOrEmpty(command))
            {
                if (consoleLog.Count == 0 || consoleLog.Last().ToLower() != command.ToLower())
                {
                    consoleLog.Add(command);

                    File.AppendAllLines(consoleLogFileName, new[] { command });
                }

                index = consoleLog.Count;
            }
        }

        public string PrevCommand()
        {
            if (index > 0)
                return consoleLog[--index];
            else
                return null;
        }

        public string NextCommand()
        {
            if (index < consoleLog.Count - 1)
                return consoleLog[++index];
            else
                return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
