﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public class TextProcessor
    {
        public List<char> SpecialCharacters { get; set; }
        public List<string> Stopwords { get; set; }
        public Dictionary<String, String> HTMLCharacters { get; set; }

        public TextProcessor(List<string> stopWords)
        {
            #region HTMLCharacters
            HTMLCharacters = new Dictionary<string, string>();
            HTMLCharacters.Add("&Aacute;", "Á");
            HTMLCharacters.Add("&aacute;", "á");
            HTMLCharacters.Add("&Acirc;", "Â");
            HTMLCharacters.Add("&acirc;", "â");
            HTMLCharacters.Add("&Agrave;", "À");
            HTMLCharacters.Add("&agrave;", "à");
            HTMLCharacters.Add("&Aring;", "Å");
            HTMLCharacters.Add("&aring;", "å");
            HTMLCharacters.Add("&Atilde;", "Ã");
            HTMLCharacters.Add("&atilde;", "ã");
            HTMLCharacters.Add("&Auml;", "Ä");
            HTMLCharacters.Add("&auml;", "ä");
            HTMLCharacters.Add("&AElig;", "Æ");
            HTMLCharacters.Add("&aelig;", "æ");
            HTMLCharacters.Add("&iuml;", "ï");
            HTMLCharacters.Add("&Eacute;", "É");
            HTMLCharacters.Add("&eacute;", "é");
            HTMLCharacters.Add("&Ecirc;", "Ê");
            HTMLCharacters.Add("&ecirc;", "ê");
            HTMLCharacters.Add("&#234;", "ê");
            HTMLCharacters.Add("&Egrave;", "È");
            HTMLCharacters.Add("&egrave;", "è");
            HTMLCharacters.Add("&Euml;", "Ë");
            HTMLCharacters.Add("&euml;", "ë");
            HTMLCharacters.Add("&ETH;", "Ð");
            HTMLCharacters.Add("&eth;", "ð");
            HTMLCharacters.Add("&otilde;", "õ");
            HTMLCharacters.Add("&Iacute;", "Í");
            HTMLCharacters.Add("&iacute;", "í");
            HTMLCharacters.Add("&Icirc;", "Î");
            HTMLCharacters.Add("&icirc;", "î");
            HTMLCharacters.Add("&Igrave;", "Ì");
            HTMLCharacters.Add("&igrave;", "ì");
            HTMLCharacters.Add("&Iuml;", "Ï");
            HTMLCharacters.Add("&Ugrave;", "Ù");
            HTMLCharacters.Add("&ugrave;", "ù");
            HTMLCharacters.Add("&Oacute;", "Ó");
            HTMLCharacters.Add("&oacute;", "ó");
            HTMLCharacters.Add("&Ocirc;", "Ô");
            HTMLCharacters.Add("&ocirc;", "ô");
            HTMLCharacters.Add("&Ograve;", "Ò");
            HTMLCharacters.Add("&ograve;", "ò");
            HTMLCharacters.Add("&Oslash;", "Ø");
            HTMLCharacters.Add("&oslash;", "ø");
            HTMLCharacters.Add("&Otilde;", "Õ");
            HTMLCharacters.Add("&Yacute;", "Ý");
            HTMLCharacters.Add("&Ouml;", "Ö");
            HTMLCharacters.Add("&ouml;", "ö");
            HTMLCharacters.Add("&quot;", "\"");
            HTMLCharacters.Add("&Uacute;", "Ú");
            HTMLCharacters.Add("&uacute;", "ú");
            HTMLCharacters.Add("&Ucirc;", "Û");
            HTMLCharacters.Add("&ucirc;", "û");
            HTMLCharacters.Add("&reg;", "®");
            HTMLCharacters.Add("&copy;", "©");
            HTMLCharacters.Add("&Uuml;", "Ü");
            HTMLCharacters.Add("&uuml;", "ü");
            HTMLCharacters.Add("&szlig;", "ß");
            HTMLCharacters.Add("&Ccedil;", "Ç");
            HTMLCharacters.Add("&ccedil;", "ç");
            HTMLCharacters.Add("&Ntilde;", "Ñ");
            HTMLCharacters.Add("&ntilde;", "ñ");
            HTMLCharacters.Add("&yacute;", "ý");
            HTMLCharacters.Add("&hellip;", "…");
            HTMLCharacters.Add("&lt;", "<");
            HTMLCharacters.Add("&gt;", ">");
            HTMLCharacters.Add("&amp;", "&");
            HTMLCharacters.Add("&THORN;", "Þ");
            HTMLCharacters.Add("&thorn;", "þ");
            HTMLCharacters.Add("&nbsp;", " ");
            HTMLCharacters.Add("&ldquo;", "“");
            HTMLCharacters.Add("&rdquo;", "”");
            HTMLCharacters.Add("&rsquo;", "'");
            HTMLCharacters.Add("&ordm;", "º");
            HTMLCharacters.Add("&ordf;", "ª");
            HTMLCharacters.Add("&#39;", "'");
            HTMLCharacters.Add("&#8211;", "–");
            HTMLCharacters.Add("&#199;", "Ç");
            HTMLCharacters.Add("&#195;", "Ã");
            HTMLCharacters.Add("&#227;", "ã");
            HTMLCharacters.Add("&#205;", "Í");
            HTMLCharacters.Add("&#202;", "Ê");
            HTMLCharacters.Add("&#194;", "Â");
            HTMLCharacters.Add("&#192;", "À");
            HTMLCharacters.Add("&#201;", "É");
            HTMLCharacters.Add("&#218;", "Ú");
            HTMLCharacters.Add("&#193;", "Á");
            HTMLCharacters.Add("&#213;", "Õ");
            HTMLCharacters.Add("&#231;", "ç");
            HTMLCharacters.Add("&#245;", "õ");
            HTMLCharacters.Add("&#237;", "í");
            HTMLCharacters.Add("&#225;", "á");
            HTMLCharacters.Add("&#233;", "é");
            HTMLCharacters.Add("&#186;", "°");
            HTMLCharacters.Add("&#176;", "°");
            HTMLCharacters.Add("&#243;", "ó");
            HTMLCharacters.Add("&#250;", "ú");
            HTMLCharacters.Add("&ndash;", "–");
            HTMLCharacters.Add("&lrm;", "");
            HTMLCharacters.Add("&#8206;", "");
            HTMLCharacters.Add("&#x2F;", "/");
            HTMLCharacters.Add("&#xC0;", "À");
            HTMLCharacters.Add("&#xC1;", "Á");
            HTMLCharacters.Add("&#xC2;", "Â");
            HTMLCharacters.Add("&#xC3;", "Ã");
            HTMLCharacters.Add("&#xC4;", "Ä");
            HTMLCharacters.Add("&#xC5;", "Å");
            HTMLCharacters.Add("&#xC6;", "Æ");
            HTMLCharacters.Add("&#xC7;", "Ç");
            HTMLCharacters.Add("&#xC8;", "È");
            HTMLCharacters.Add("&#xC9;", "É");
            HTMLCharacters.Add("&#xCA;", "Ê");
            HTMLCharacters.Add("&#xCB;", "Ë");
            HTMLCharacters.Add("&#xCC;", "Ì");
            HTMLCharacters.Add("&#xCD;", "Í");
            HTMLCharacters.Add("&#xCE;", "Î");
            HTMLCharacters.Add("&#xCF;", "Ï");
            HTMLCharacters.Add("&#xD0;", "Ð");
            HTMLCharacters.Add("&#xD1;", "Ñ");
            HTMLCharacters.Add("&#xD2;", "Ò");
            HTMLCharacters.Add("&#xD3;", "Ó");
            HTMLCharacters.Add("&#xD4;", "Ô");
            HTMLCharacters.Add("&#xD5;", "Õ");
            HTMLCharacters.Add("&#xD6;", "Ö");
            HTMLCharacters.Add("&#xD8;", "Ø");
            HTMLCharacters.Add("&#xD9;", "Ù");
            HTMLCharacters.Add("&#xDA;", "Ú");
            HTMLCharacters.Add("&#xDB;", "Û");
            HTMLCharacters.Add("&#xDC;", "Ü");
            HTMLCharacters.Add("&#xDD;", "Ý");
            HTMLCharacters.Add("&#xDE;", "Þ");
            HTMLCharacters.Add("&#xDF;", "ß");
            HTMLCharacters.Add("&#xE0;", "à");
            HTMLCharacters.Add("&#xE1;", "á");
            HTMLCharacters.Add("&#xE2;", "â");
            HTMLCharacters.Add("&#xE3;", "ã");
            HTMLCharacters.Add("&#xE4;", "ä");
            HTMLCharacters.Add("&#xE5;", "å");
            HTMLCharacters.Add("&#xE6;", "æ");
            HTMLCharacters.Add("&#xE7;", "ç");
            HTMLCharacters.Add("&#xE8;", "è");
            HTMLCharacters.Add("&#xE9;", "é");
            HTMLCharacters.Add("&#xEA;", "ê");
            HTMLCharacters.Add("&#xEB;", "ë");
            HTMLCharacters.Add("&#xEC;", "ì");
            HTMLCharacters.Add("&#xED;", "í");
            HTMLCharacters.Add("&#xEE;", "î");
            HTMLCharacters.Add("&#xEF;", "ï");
            HTMLCharacters.Add("&#xF1;", "ñ");
            HTMLCharacters.Add("&#xF2;", "ò");
            HTMLCharacters.Add("&#xF3;", "ó");
            HTMLCharacters.Add("&#xF4;", "ô");
            HTMLCharacters.Add("&#xF5;", "õ");
            HTMLCharacters.Add("&#xF6;", "ö");
            HTMLCharacters.Add("&#xF8;", "ø");
            HTMLCharacters.Add("&#xF9;", "ù");
            HTMLCharacters.Add("&#xFA;", "ú");
            HTMLCharacters.Add("&#xFB;", "û");
            HTMLCharacters.Add("&#xFC;", "ü");
            HTMLCharacters.Add("&#xFD;", "ý");
            HTMLCharacters.Add("&#xFE;", "þ");
            HTMLCharacters.Add("&#xFF;", "ÿ");

            #endregion

            #region SpecialCharacters
            SpecialCharacters = new List<char>();
            SpecialCharacters.Add('.');
            SpecialCharacters.Add(',');
            SpecialCharacters.Add('!');
            SpecialCharacters.Add('?');
            SpecialCharacters.Add('#');
            SpecialCharacters.Add('@');
            SpecialCharacters.Add('$');
            SpecialCharacters.Add('%');
            SpecialCharacters.Add('/');
            SpecialCharacters.Add('\u003c');
            SpecialCharacters.Add('\u003e');
            SpecialCharacters.Add('-');
            SpecialCharacters.Add('=');
            SpecialCharacters.Add('.');
            SpecialCharacters.Add('_');
            SpecialCharacters.Add(']');
            SpecialCharacters.Add('[');
            SpecialCharacters.Add('{');
            SpecialCharacters.Add('}');
            SpecialCharacters.Add('(');
            SpecialCharacters.Add(')');
            SpecialCharacters.Add('*');
            SpecialCharacters.Add(';');
            SpecialCharacters.Add('|');
            SpecialCharacters.Add('\\');
            SpecialCharacters.Add('+');
            SpecialCharacters.Add(';');
            SpecialCharacters.Add('"');
            SpecialCharacters.Add('\'');
            #endregion

            #region Stopwords
            Stopwords = stopWords;
            #endregion
        }

        public string PreProcess(string text)
        {
            text = text.ToLower();
            text = RemoveAccents(text);
            text = RemoveSpecialCharacters(text);
            text = RemoveExtraWhiteSpaces(text);
            text = RemoveStopwords(text);
            text = RemoveSmallWords(text);

            return text;
        }

        public string ReplaceHTMLCharacters(string text)
        {
            foreach (var item in HTMLCharacters)
                text = text.Replace(item.Key, item.Value);

            return text;
        }

        public string RemoveLineBreaksTabs(string text)
        {
            return text.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");
        }

        // replaces special characters with empty spaces
        private string RemoveSpecialCharacters(string text)
        {
            foreach (var specialCharacter in SpecialCharacters)
                text = text.Replace(specialCharacter, ' ');

            return text;
        }

        // replaces stopwords with empty spaces
        private string RemoveStopwords(string text)
        {
            if (Stopwords == null)
                return text;

            var sb = new StringBuilder();

            foreach (var word in text.Split(' '))
            {
                if (!Stopwords.Contains(word))
                {
                    sb.Append(word);
                    sb.Append(" ");
                }
            }

            return sb.ToString().TrimEnd();
        }

        // replaces accented letters with corresponding unaccented letters
        public string RemoveAccents(string text)
        {
            string accented = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            string unaccented = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";
            for (int i = 0; i < accented.Length; i++)
            {
                text = text.Replace(accented[i].ToString(), unaccented[i].ToString());
            }
            return text;
        }

        // trim text and remove extra white spaces between words
        public string RemoveExtraWhiteSpaces(string text)
        {
            text = text.Trim();

            var sb = new StringBuilder();

            var n = text.Count();

            for (var i = 0; i < n; i++)
            {
                var c = text[i];

                if (Char.IsWhiteSpace(c))
                {
                    if (!Char.IsWhiteSpace(text[i + 1]))
                        sb.Append(c);
                }
                else
                    sb.Append(c);
            }

            return sb.ToString();
        }

        // removes words containing less than 4 letters
        private string RemoveSmallWords(string text)
        {
            var sb = new StringBuilder();

            foreach (var word in text.Split(' '))
            {
                if (word.Count() > 2)
                {
                    sb.Append(word);
                    sb.Append(" ");
                }
            }

            return sb.ToString().TrimEnd();
        }

        public string RemoveTags(string texto)
        {
            char[] array = new char[texto.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < texto.Length; i++)
            {
                char let = texto[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}
