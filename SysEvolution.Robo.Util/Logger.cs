﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public class Logger
    {
        private static readonly object _lock = new object();

        public static void Log(Exception ex, string message = null)
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(message))
                sb.AppendLine(message);
            
            sb.AppendLine(ex.ToString());

            while (ex.InnerException != null)
            {
                sb.AppendLine(ex.InnerException.ToString());
                ex = ex.InnerException;
            }

            InnerLog("Logs", sb.ToString());
        }

        public static void Log(AggregateException ex, string message = null)
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(message))
                sb.AppendLine(message);

            foreach (var innerException in ex.Flatten().InnerExceptions)
                sb.AppendLine(ex.ToString());

            InnerLog("Logs", sb.ToString());
        }

        public static void Log(string message)
        {
            InnerLog("Logs", message);
        }

        public static void Log(string message, params object[] args)
        {
            InnerLog("Logs", string.Format(message, args));
        }

        private static void InnerLog(string folder, string message, int? slot = null)
        {
            var now = DateTime.Now;

            var sb = new StringBuilder();
            sb.Append(now.ToString("dd/MM/yyyy HH:mm:ss"));
            if (slot.HasValue)
                sb.Append(" - [" + slot.Value + "] ");
            else
                sb.Append(" - ");
            sb.AppendLine(message);

            TextAppended?.Invoke(null, sb.ToString());

            Console.WriteLine(sb.ToString());
        }

        #region Lot slot
        public static void LogSlot(int slot, Exception ex, string message = null)
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(message))
                sb.AppendLine(message);

            sb.AppendLine(ex.ToString());

            while (ex.InnerException != null)
            {
                sb.AppendLine(ex.InnerException.ToString());
                ex = ex.InnerException;
            }

            InnerLog(Path.Combine(ConfigurationManager.AppSettings["Root"], "Logs"), sb.ToString(), slot);
        }

        public static void LogSlot(int slot, AggregateException ex, string message = null)
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(message))
                sb.AppendLine(message);

            foreach (var innerException in ex.Flatten().InnerExceptions)
                sb.AppendLine(ex.ToString());

            InnerLog(Path.Combine(ConfigurationManager.AppSettings["Root"], "Logs"), sb.ToString(), slot);
        }

        public static void LogSlot(int slot, string message)
        {
            InnerLog(Path.Combine(ConfigurationManager.AppSettings["Root"], "Logs"), message, slot);
        }

        public static void LogSlot(int slot, string message, params object[] args)
        {
            InnerLog(Path.Combine(ConfigurationManager.AppSettings["Root"], "Logs"), string.Format(message, args), slot);
        }
        #endregion

        public static event EventHandler<string> TextAppended;
    }
}
