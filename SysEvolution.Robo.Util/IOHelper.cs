﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public static class IOHelper
    {
        private static object _lock = new object();

        public static string NewFolder(string path, string folderName)
        {
            var fullPath = path + folderName;

            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);

            return fullPath;
        }

        /// <summary>
        /// Obtém o arquivo json e retorna um tipo genérico.
        /// </summary>
        /// <typeparam name="T">Tipo convertido</typeparam>
        /// <param name="fileName">Caminho completo do arquivo</param>
        /// <returns></returns>
        public static T GetJson<T>(string fileName)
        {
            if (File.Exists(fileName))
            {
                while (true)
                {
                    try
                    {
                        var json = File.ReadAllText(fileName);
                        return JsonConvert.DeserializeObject<T>(json);
                    }
                    catch (IOException e)
                    {
                        Thread.Sleep(100);
                    }
                }
            }

            return default(T);
        }

        public static void SaveJson(string fileName, object json)
        {
            while (true)
            {
                try
                {
                    File.WriteAllText(fileName, JsonConvert.SerializeObject(json, Newtonsoft.Json.Formatting.Indented));
                    break;
                }
                catch (IOException e)
                {

                    Thread.Sleep(100);
                }
            }
        }

        /// <summary>
        /// Obtém os nomes de todos os arquivos da pasta especificada
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ext">Extensão</param>
        /// <returns></returns>
        public static List<string> GetFileNames(string path, string ext)
        {
            ext = (ext.Contains(".")) ? ext : "." + ext;
            var files = Directory.GetFiles(path);
            return files.Where(f => f.EndsWith(ext)).Select(f => Path.GetFileNameWithoutExtension(new FileInfo(f).Name)).ToList();
        }
    }
}
