﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public static class HttpHelper
    {
        public static dynamic Post(string url, string post)
        {
            dynamic result = null;
            var postBody = Encoding.UTF8.GetBytes(post);
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = postBody.Length;
            request.Timeout = 10 * 1000;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(postBody, 0, postBody.Length);
                stream.Close();
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                result = JsonConvert.DeserializeObject(streamReader.ReadToEnd());

                response.Close();
            }

            return result;
        }
    }
}
