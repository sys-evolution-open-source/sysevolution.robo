﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public class FileManager
    {
        public static Dictionary<string, string> GetFileHashes()
        {
            var hashes = new Dictionary<string, string>();

            var root = ConfigurationManager.AppSettings["Root"];
            var repoFolders = ConfigurationManager.AppSettings["RepoFolders"].Split(';');

            using (var md5 = MD5.Create())
            {
                foreach (var repoFolder in repoFolders)
                {
                    if (!string.IsNullOrEmpty(repoFolder))
                    {
#if Docker
                        var folder = Path.Combine(root, repoFolder.Replace('\\', '/') );
#else
                        var folder = Path.Combine(root, repoFolder);
#endif


                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                            break;
                        }

                        var files = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories);

                        foreach (var file in files)
                        {
                            var fileInfo = new FileInfo(file);

                            using (var stream = File.OpenRead(file))
                            {
                                var hash = md5.ComputeHash(stream);
                                var hashStr = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();

#if Docker
                                var fileKey = file.Replace(root, "").Substring(1).Replace("/", "\\");
                                hashes.Add(fileKey, hashStr);
#else
                                hashes.Add(file.Replace(root, "").Substring(1), hashStr);
#endif

                            }
                        }
                    }
                }
            }

            return hashes;
        }

        public static Dictionary<string, string> GetFileHashes(List<FileInfo> files)
        {
            var hashes = new Dictionary<string, string>();

            using (var md5 = MD5.Create())
            {
                foreach (var file in files)
                {
                    using (var stream = File.OpenRead(file.FullName))
                    {
                        var hash = md5.ComputeHash(stream);
                        var hashStr = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                        hashes.Add(file.Name, hashStr);
                    }
                }
            }

            return hashes;
        }

        public static byte[] ZipFiles(Dictionary<string, byte[]> files)
        {
            byte[] zipFile = null;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (var file in files)
                    {
                        var entry = zipArchive.CreateEntry(file.Key);

                        using (Stream stream = entry.Open())
                        {
                            stream.Write(file.Value, 0, file.Value.Length);
                        }
                    }
                }

                zipFile = memoryStream.ToArray();
            }

            return zipFile;
        }

        public static Dictionary<string, byte[]> UnzipFiles(byte[] zipFile)
        {
            var files = new Dictionary<string, byte[]>();

            using (MemoryStream memoryStream = new MemoryStream(zipFile))
            {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Read, true))
                {
                    foreach (var entry in zipArchive.Entries)
                    {
                        var content = new byte[entry.Length];
                        entry.Open().Read(content, 0, (int)entry.Length);

                        files.Add(entry.FullName, content);
                    }
                }
            }

            return files;
        }
    }
}
