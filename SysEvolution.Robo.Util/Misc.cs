﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public class Misc
    {
        private static Random random = new Random();
        public static string RandomString(int length = 5)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GetClientIP(HttpRequestMessage request)
        {
            var ip = request.GetOwinContext().Request.RemoteIpAddress;
            if (ip == "::1") ip = "127.0.0.1";
            return ip;
        }

        public static Dictionary<K, V> ToDictionary<K, V>(JToken jToken)
        {
            IDictionary<string, JToken> tokens = (JObject)jToken;
            return tokens.ToDictionary(_ => (K)Convert.ChangeType(_.Key, typeof(K)), _ => (V)Convert.ChangeType(_.Value, typeof(V)));
        }
        public static bool IsNumber(string value)
        {
            int n;
            return int.TryParse(value, out n);
        }

        public static byte[] ImageToByteArray(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public static Rectangle? GetWidthPositionImage(string data, Size s)
        {
            Rectangle rec = new Rectangle();

            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    rec = new Rectangle();
                    var splited = data.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    rec.X = Convert.ToInt32(splited[0]);
                    rec.Y = Convert.ToInt32(splited[1]);
                    rec.Width = Convert.ToInt32(splited[2]);
                    rec.Height = Convert.ToInt32(splited[3]);

                    var tempWidth = Convert.ToInt32(splited[4]);
                    var tempHeight = Convert.ToInt32(splited[5]);
                    if (s.Width > tempWidth)
                    {
                        rec.Width = (int)(((double)s.Width / tempWidth) * rec.Width);
                        rec.Height = (int)(((double)s.Height / tempHeight) * rec.Height);
                    }

                    return rec;
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return null;
        }

        //Resize
        public static Bitmap BitmapTreatment(Bitmap bmp)
        {
            Bitmap bmap = (Bitmap)bmp;
            bmap = SetGrayscale(bmap);
            bmap = RemoveNoise(bmap);

            return bmap;
        }


        //SetGrayscale
        public static Bitmap SetGrayscale(Bitmap img)
        {

            Bitmap temp = (Bitmap)img;
            Bitmap bmap = (Bitmap)temp.Clone();
            Color c;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    c = bmap.GetPixel(i, j);
                    byte gray = (byte)(.299 * c.R + .587 * c.G + .114 * c.B);

                    bmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            return (Bitmap)bmap.Clone();

        }
        //RemoveNoise
        public static Bitmap RemoveNoise(Bitmap bmap)
        {

            for (var x = 0; x < bmap.Width; x++)
            {
                for (var y = 0; y < bmap.Height; y++)
                {
                    var pixel = bmap.GetPixel(x, y);
                    if (pixel.R < 162 && pixel.G < 162 && pixel.B < 162)
                        bmap.SetPixel(x, y, Color.Black);
                    else if (pixel.R > 162 && pixel.G > 162 && pixel.B > 162)
                        bmap.SetPixel(x, y, Color.White);
                }
            }

            return bmap;
        }
    }
}
