﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Util
{
    public class APIConfig : ConfigurationSection
    {
        public static APIConfig GetConfig(string name)
        {
            return ConfigurationManager.GetSection(name) as APIConfig;
        }
        
        [ConfigurationProperty("address")]
        public string Address
        {
            get { return this["address"] as string; }
        }

        [ConfigurationProperty("key")]
        public string Key
        {
            get{ return this["key"] as string; }
        }
    }
}
