﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using Patagames.Ocr;
using SysEvolution.Robo.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

namespace SysEvolution.Robo.Worker.Core
{
    public abstract class Spider
    {
        protected int defaultTimeOut = 0;
        protected TextProcessor textProcessor;

        protected string defaultEncoding;

        public DateTime InicioExecucao { get; set; }
        public int? TempoExecucao { get; set; }

        protected SpiderConfiguration config;

        public string Name
        {
            get { return config.ScriptName; }
        }

        public string SiteName
        {
            get { return config.SiteName; }
            set { config.SiteName = value; }
        }

        public JObject Parametros { get; set; }

        protected Spider()
        {
            config = new SpiderConfiguration();

            config.ScriptName = this.GetType().Name;
        }

        protected virtual void Configure()
        {

        }

        protected void Initialize()
        {
            HtmlNode.ElementsFlags.Remove("form");
        }

        protected abstract void Run();

        public void Start()
        {
            Configure();

            try
            {
                InicioExecucao = DateTime.Now;
                Run();
            }
            catch (SafeException se)
            {
            }
            catch (WebDriverException ex)
            {
                try
                {
                    OnError();
                }
                catch (Exception ed) 
                {
                    Logger.Log("Erro no Script: " + config.ScriptName + "\n" + ed.Message + "------>" + ed.StackTrace); 
                }
                throw;
            }
            catch (AggregateException e)
            {
                try
                {
                    OnError();
                }
                catch (Exception ed) 
                {
                    Logger.Log(ed.Message + "------>" + ed.StackTrace); 
                }
                throw;
            }
            catch (Exception e)
            {
                try
                {
                    OnError();
                }
                catch (Exception ed) 
                {
                    Logger.Log(ed.Message + "------>" + ed.StackTrace); 
                }
                throw;
            }
            finally
            {
            }
        }

        protected virtual bool Timeout()
        {
            if (!TempoExecucao.HasValue)
                return false;

            var tempoExecucaoAtual = DateTime.Now.Subtract(InicioExecucao).Minutes;

            return tempoExecucaoAtual > TempoExecucao.Value;
        }

        public virtual void OnError()
        {

        }

        public HtmlDocument GetHtml(string url)
        {
            var html = new HtmlDocument();

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; //ignora certificados de autencidades;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);


            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36";
                response = (HttpWebResponse)request.GetResponse();
            }

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (string.IsNullOrEmpty(defaultEncoding))
                {
                    if (string.IsNullOrEmpty(response.CharacterSet))
                        readStream = new StreamReader(receiveStream);
                    else
                        readStream = (response.CharacterSet.Contains(',')) ? new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet.Split(',').First()))
                            : new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(defaultEncoding));
                }

                string content = readStream.ReadToEnd();
                html.LoadHtml(content);

                response.Close();
                readStream.Close();
            }

            return html;
        }

        #region Misc.

        public void LoggerLog(string message)
        {
            Logger.Log(message);
        }

        public void Log(string content)
        {
            Console.WriteLine(content);
        }

        #endregion
    }

    [Serializable]
    public class SafeException : Exception { }

    public class SpiderConfiguration
    {
        // Diretórios
        public string ScriptName { get; set; }
        public string SiteName { get; set; }
    }
}
