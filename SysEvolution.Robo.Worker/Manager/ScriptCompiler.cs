﻿using Microsoft.CSharp;
using SysEvolution.Robo.Util;
using SysEvolution.Robo.Worker.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Worker.Manager
{
    public class ScriptCompiler
    {
        public T Compile<T>(Script script)
        {
            var filePath = Path.Combine(ConfigurationManager.AppSettings["Root"], "Scripts", script.Tipo.ToString(), script.Nome + ".cs");

            var content = File.ReadAllText(filePath);

            var provider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } });

            var parameters = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false,
            };

            parameters.ReferencedAssemblies.Add("System.dll");
            parameters.ReferencedAssemblies.Add("HtmlAgilityPack.CssSelectors.dll");
            parameters.ReferencedAssemblies.Add("HtmlAgilityPack.dll");
            parameters.ReferencedAssemblies.Add("Newtonsoft.Json.dll");
            parameters.ReferencedAssemblies.Add("WebDriver.dll");
            parameters.ReferencedAssemblies.Add("WebDriver.Support.dll");
            parameters.ReferencedAssemblies.Add("System.Linq.dll");
            parameters.ReferencedAssemblies.Add("System.Xml.dll");
            parameters.ReferencedAssemblies.Add("System.Core.dll");
            parameters.ReferencedAssemblies.Add("System.Web.dll");
            parameters.ReferencedAssemblies.Add("System.IO.dll");
            parameters.ReferencedAssemblies.Add("Patagames.Ocr.dll");
            parameters.ReferencedAssemblies.Add("System.Configuration.dll");
            parameters.ReferencedAssemblies.Add("SysEvolution.Robo.Util.dll");

            string exeName = Assembly.GetEntryAssembly().Location;
            parameters.ReferencedAssemblies.Add(exeName);

            CompilerResults results = provider.CompileAssemblyFromSource(parameters, content);

            if (results.Errors.HasErrors)
            {
                for (var i = 0; i < results.Errors.Count; i++)
                {
                    var error = results.Errors[i];

                    Logger.Log("Ocorreu um erro ao compilar o script " + script.Nome + ": " + error.ErrorText);
                }
            }

            var scriptNamespace = GetNamespace(content);

            if (script.Nome != null)
            {
                var instance = (T)results.CompiledAssembly.CreateInstance(scriptNamespace + "." + script.Nome);

                return instance;
            }
            else
                return default(T);
        }

        private string GetNamespace(string script)
        {
            var regex = new Regex(@".*?namespace\s*(?<Namespace>.*?)\s", RegexOptions.Multiline);

            var match = regex.Match(script);

            if (match.Success)
                return match.Groups["Namespace"].Value.Trim();

            return null;
        }
    }
}
