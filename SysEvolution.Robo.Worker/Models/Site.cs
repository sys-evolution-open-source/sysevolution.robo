﻿using Newtonsoft.Json.Linq;
using SysEvolution.Robo.Worker.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Worker.Models
{
    public class Site
    {
        public string IdExecucao { get; set; }
        public int IdSite { get; set; }
        public string Nome { get; set; }
        public int Prioridade { get; set; }
        public DateTime DataHoraInicioExecucao { get; set; }
        public DateTime DataHoraFimExecucao { get; set; }
        public int MaxTentativas { get; set; }
        public int Tentativas { get; set; }
        public Script Script { get; set; }
        public JObject Parametros { get; set; }
        public int? IdSiteDisposable { get; set; }
        public JArray BaseHomologacao { get; set; }
        public int? TempoExecucao { get; set; }

        public Spider Spider { get; set; }
    }
}
