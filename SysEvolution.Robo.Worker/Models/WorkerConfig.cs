﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Worker.Models
{
    public class WorkerConfig
    {
        public int IdWorker { get; set; }
        public int MaxThreads { get; set; }
        public bool Ativo { get; set; }
        public int IntervaloSinalizacao { get; set; }
        public List<int> IdSites { get; set; }
    }
}
