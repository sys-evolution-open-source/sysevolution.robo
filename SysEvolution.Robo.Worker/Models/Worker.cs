﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Worker.Models
{
    public class Worker
    {
        public int idWorker { get; set; }
        public double ram { get; set; }
        public string cpu { get; set; }
        public double cpuUsage { get; set; }
        public double ramUsage { get; set; }
        public List<int> idSites { get; set; }
    }
}
