﻿using SysEvolution.Robo.Worker.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysEvolution.Robo.Worker.Models
{
    public enum TipoScript { Edital, Homologacao }

    public class Script
    {
        public TipoScript Tipo { get; set; }
        public string Nome { get; set; }
    }
}
